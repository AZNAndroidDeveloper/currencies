package uz.azn.currencies

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.azn.currencies.databinding.ActivityMainBinding
import uz.azn.currencies.currencies.CurrenciesFragment

class MainActivity : AppCompatActivity() {
    private  val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().replace(binding.frameLayout.id, CurrenciesFragment()).commit()
    }
}