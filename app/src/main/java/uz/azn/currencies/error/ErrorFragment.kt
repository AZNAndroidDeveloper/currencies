package uz.azn.currencies.error

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import uz.azn.currencies.R
import uz.azn.currencies.databinding.FragmentErrorBinding

class ErrorFragment : Fragment(R.layout.fragment_error) {
  private lateinit var binding:FragmentErrorBinding
  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
  binding = FragmentErrorBinding.bind(view)
    with(binding){
      btnExit.apply {
        setOnClickListener {
          activity!!.recreate()
        }
      }
    }
  }
}