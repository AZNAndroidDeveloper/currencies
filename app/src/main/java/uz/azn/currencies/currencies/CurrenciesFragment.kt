package uz.azn.currencies.currencies

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.*
import android.net.NetworkCapabilities.*
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.baoyz.widget.PullRefreshLayout
import com.google.android.material.transition.Hold
import org.json.JSONException
import uz.azn.currencies.R
import uz.azn.currencies.adapter.CurrenciesAdapter
import uz.azn.currencies.currencies.model.Currencies
import uz.azn.currencies.databinding.FragmentCurrenciesBinding
import uz.azn.currencies.error.ErrorFragment
import java.util.*
import javax.xml.transform.ErrorListener

const val URL = "https://cbu.uz/ru/arkhiv-kursov-valyut/json/"

class CurrenciesFragment : Fragment(R.layout.fragment_currencies) {
    private lateinit var binding: FragmentCurrenciesBinding
    private var requestQueue: RequestQueue? = null

    private lateinit var progressDialog: ProgressDialog
    private val currenciesAdapter = CurrenciesAdapter()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCurrenciesBinding.bind(view)
        if (hasInternetConnection()) {
            progressDialog = ProgressDialog(requireContext())
            progressDialog.setMessage("Please wait ....")
            progressDialog.setCanceledOnTouchOutside(false)
            progressDialog.show()
            requestQueue = Volley.newRequestQueue(requireContext())
            jsonParse()
            with(binding) {

            }
        } else {
            fragmentManager!!.beginTransaction().replace(R.id.frame_layout,ErrorFragment()).commit()
//            val builder = AlertDialog.Builder(requireContext())
//                .setTitle("Internet connection")
//                .setCancelable(false)
//                .setMessage("Please check your connection and try again ...")
//                .setPositiveButton("Exit") { dialog, which ->
//                    dialog.dismiss()
//                    activity!!.finish()
//                }
//                .setNegativeButton("Retry") { dialog, which ->
//                    dialog.dismiss()
//                    activity!!.recreate()
//                }
//            val dialog = builder.create()
//            dialog.show()
        }
    }

    private fun jsonParse() {
        val currenciesList = mutableListOf<Currencies>()
        val request =
            JsonArrayRequest(Request.Method.GET, URL, null, Response.Listener { response ->
                try {
                    for (i in 0 until response.length()) {
                        val currencies = response.getJSONObject(i)
                        val uzName = currencies.getString("CcyNm_UZ")
                        val enName = currencies.getString("CcyNm_EN")
                        val rate = currencies.getString("Rate")
                        val date = currencies.getString("Date")
                        val ccy = currencies.getString("Ccy")
                        currenciesList.add(Currencies(uzName, enName, rate, date, ccy))
                    }
                    currenciesAdapter.setElement(currenciesList)
                    Log.d("TAG", "jsonParse: $currenciesList")
                    with(binding) {
                        recycler.apply {
                            layoutManager = LinearLayoutManager(requireContext())
                            adapter = currenciesAdapter
                        }
                        refreshLayout.apply {
                            setOnRefreshListener {
                                Handler().postDelayed({
                                    currenciesList.shuffle()
                                    currenciesAdapter.setElement(currenciesList)
                                    currenciesAdapter.notifyDataSetChanged()
                                    refreshLayout.setRefreshing(false)
                                }, 1000)
                            }
                            setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN)
                        }
                    }
                    progressDialog.dismiss()
//                    Handler().postDelayed({
//
//                    }, 1000)

                } catch (ex: JSONException) {
                    ex.printStackTrace()
                }

            }, Response.ErrorListener { error ->
                error.printStackTrace()

            })
        val queue = Volley.newRequestQueue(requireContext())
        queue.add(request)


    }

    fun hasInternetConnection(): Boolean {
        val connectivityManager =
            activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork)
                ?: return false
            return when {
                capabilities.hasTransport(TRANSPORT_WIFI) -> true
                capabilities.hasTransport(TRANSPORT_CELLULAR) -> true
                capabilities.hasTransport(TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.activeNetworkInfo?.run {
                return when (type) {
                    TYPE_WIFI -> true
                    TYPE_MOBILE -> true
                    TYPE_ETHERNET -> true
                    else -> false

                }
            }
        }
        return false
    }
}