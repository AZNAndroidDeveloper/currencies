package uz.azn.currencies.currencies.model

data class Currencies(val uzName:String, val enName:String, val rate:String, val date:String, val ccy:String) {
}