package uz.azn.currencies.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.azn.currencies.currencies.model.Currencies
import uz.azn.currencies.databinding.CurrenciesItemBinding as CurrenciesBinding

class CurrenciesAdapter:RecyclerView.Adapter<CurrenciesAdapter.CurrenciesViewHolder>() {
    private val currenciesList = mutableListOf<Currencies>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrenciesViewHolder = CurrenciesViewHolder(CurrenciesBinding.inflate(LayoutInflater.from(parent.context),parent,false))


    override fun getItemCount(): Int  = currenciesList.size

    override fun onBindViewHolder(holder: CurrenciesViewHolder, position: Int) {
        val element = currenciesList[position]
        holder.onBind(element)
    }

    inner  class CurrenciesViewHolder(val binding: CurrenciesBinding):RecyclerView.ViewHolder(binding.root){

        @SuppressLint("SetTextI18n")
        fun onBind(element:Currencies){
            with(binding){
                tvUzName.text = element.uzName
                tvEnName.text = element.enName
                tvRate.text = "1 ${element.ccy} = ${element.rate} so'm"
                tvDate.text = element.date
            }

        }
    }
    fun setElement(element:MutableList<Currencies>){
        currenciesList.apply { clear(); addAll(element) }
    }
}